module gitlab.com/ajwalker/runner-backbench

go 1.22rc2

require (
	github.com/expr-lang/expr v1.16.0
	gopkg.in/yaml.v3 v3.0.1
)
