package main

import (
	"context"
	"embed"
	"errors"
	"flag"
	"fmt"
	"io/fs"
	"os"
	"os/signal"

	"gitlab.com/ajwalker/runner-backbench/cmd"
)

//go:embed jobs/*.yaml
var content embed.FS

type Command interface {
	Command() (fs *flag.FlagSet, usage string)
	Execute(ctx context.Context) error
}

type Commands []Command

func (c Commands) Usage() {
	for _, cmd := range c {
		fs, usage := cmd.Command()
		fmt.Fprintln(fs.Output(), fs.Name(), usage)
		fs.PrintDefaults()
	}
	os.Exit(1)
}

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	sub, err := fs.Sub(content, "jobs")
	if err != nil {
		panic(err)
	}

	cmds := Commands{
		cmd.NewServeCmd(sub),
		cmd.NewConvertCmd(sub),
	}

	if len(os.Args) < 2 {
		cmds.Usage()
	}

	for _, cmd := range cmds {
		fs, usage := cmd.Command()
		if os.Args[1] == fs.Name() {
			fs.Parse(os.Args[2:])
			if err := cmd.Execute(ctx); err != nil {
				if errors.Is(err, flag.ErrHelp) {
					fmt.Fprintln(fs.Output(), fs.Name(), usage)
					fs.PrintDefaults()
					os.Exit(1)
				}

				panic(err)
			}
			return
		}
	}

	cmds.Usage()
}
