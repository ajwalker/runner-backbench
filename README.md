# Runner Backbench

Runner Backbench is a tool to run a collection of jobs against a GitLab Runner
implementation to benchmark and test for uniform runtime behaviour between
different runner configurations.

It mimics GitLab by dispatching jobs to a Runner, complete with an API for the
transmission of logs and the delivery of artifacts.

Each job test includes the task to be executed, compatible runner configurations
(identified through tag sets), and specific criteria that must be satisfied for
the test to be considered successful (such as matching trace output).

## Assertions

In order for a job to pass certain conditions need to be met. The supported checks for
each job are:

- `match`: an array of regex patterns to match on the trace.
- `includes`: an array of text strings to find within the trace.
- `excludes`: an array of text strings that must not be found within the trace.
- `exit_code`: the exit code when a job is terminated with a failure.
- `failure_reason`: the failure reason provided by runner when a job is terminated with a failure.

## Trace commands

To control the server side behaviour of a job, such as canceling/aborting, commands
can be added to the trace output from a script.

The supports commands are:

- `echo "${backbench_abort_func}"` - This will echo a specific command that can trigger an abort. This can be argumented with: `echo "${backbench_abort_func}:<search string>"` to match text before triggering an abort.
- `echo "${backbench_cancel_func}"` - This will echo a specific command that can trigger a cancel. This can be argumented with: `echo "${backbench_cancel_func}:<search string>"` to match text before triggering a cancel.

## Expression language

The YAML configuration supports [expr-lang](https://expr-lang.org/) and is done via the `!expr` tag.

The variables `!expr` has available to it are from the job requests `VersionInfo` struct, so includes
the name, version, revision, platform, architecture, executor, shell and features of the Runner.

## Example test

Job tests are spread across appropriately named `*.yaml` files for the category/type of test. 

Each test file can have one or more tests.

`cancel.yaml`:

```yaml
during script phase: # name of test
  runner: # match the VersionInfo struct for this test to run
    features:
      cancelable: true # only run on Runners supporting the 'cancelable' feature
  job:
    steps:
      - name: script
        script:
          - echo "${backbench_cancel_func}" # special command to cancel job
          - sleep 60
      - name: after_script
        script:
          - echo "I am after script, and I am running!"
    artifacts:
      - paths: [path]
        when: on_failure
  includes:
    - "I am after script, and I am running!" # We expect after_script to execute even on cancel with the cancelable feature
  failure_reason: script_failure

  # we use the expression language as different executors have different exit codes when cancelled
  exit_code: !expr |
    { 'shell': -1, 'docker': 143 }[executor]
```

## Using

When backbench is running, it defaults to listening on "localhost:8085". You can connect a runner with a specific configuration to it with something like this runner `config.toml`:

```toml
concurrent = 1000

[[runners]]
  name = "my-runner"
  url = "http://localhost:8085"
  token = "glrt-literally-any-token-will-do"
  executor = "docker"
  shell = "bash"
  [runners.docker]
    image = "busybox:latest"
    volumes = ["/cache"]
```

### Run all tests

```shell
go run main.go serve
```

### Run one specific test

Test names are based on the `<test filename>/test_name` where spaces are replaced with underscores.

The `-run` argument loads tests based on a regex pattern:

```shell
go run main.go serve -run "cancel/during_script"
```

### Run tests/specific test more than once

```shell
go run main.go serve -run "cancel/during_script" -count 100
```
