package cmd

import (
	"context"
	"flag"
	"fmt"
	"io/fs"

	"gitlab.com/ajwalker/runner-backbench/pkg/config"
)

type convertCmd struct {
	fs *flag.FlagSet

	addr string
	run  string

	fsys fs.FS
}

func NewConvertCmd(fsys fs.FS) *convertCmd {
	c := &convertCmd{}
	c.fs = flag.NewFlagSet("convert", flag.ExitOnError)

	c.fs.StringVar(&c.run, "run", "", "test to match")

	c.fsys = fsys

	return c
}

func (cmd *convertCmd) Command() (*flag.FlagSet, string) {
	return cmd.fs, ""
}

func (cmd *convertCmd) Execute(ctx context.Context) error {
	filtered := filteredTests(cmd.run, cmd.fsys, 1)

	fmt.Println(config.ConvertToGitLab(filtered))

	return nil
}
