package cmd

import (
	"context"
	"flag"
	"fmt"
	"io/fs"
	"net/http"
	"regexp"

	"gitlab.com/ajwalker/runner-backbench/pkg/api"
	"gitlab.com/ajwalker/runner-backbench/pkg/config"
)

type serveCmd struct {
	fs *flag.FlagSet

	addr  string
	run   string
	count int

	fsys fs.FS
}

func NewServeCmd(fsys fs.FS) *serveCmd {
	c := &serveCmd{}
	c.fs = flag.NewFlagSet("serve", flag.ExitOnError)

	c.fs.StringVar(&c.addr, "addr", "localhost:8085", "server listen address")
	c.fs.StringVar(&c.run, "run", "", "test to match")
	c.fs.IntVar(&c.count, "count", 1, "number of times to run selected tests")

	c.fsys = fsys

	return c
}

func (cmd *serveCmd) Command() (*flag.FlagSet, string) {
	return cmd.fs, ""
}

func (cmd *serveCmd) Execute(ctx context.Context) error {
	filtered := filteredTests(cmd.run, cmd.fsys, cmd.count)

	s := api.New(cmd.addr, filtered)

	fmt.Printf("listening: %s\n", cmd.addr)

	server := &http.Server{Addr: cmd.addr, Handler: s}
	defer server.Close()

	ctx, cancel := context.WithCancel(ctx)
	go func() {
		defer cancel()
		fmt.Println(server.ListenAndServe())
	}()

	<-ctx.Done()

	return nil
}

func filteredTests(run string, sub fs.FS, count int) []config.TestBlueprint {
	re := regexp.MustCompile(run)

	allTests, err := config.Load(sub)
	if err != nil {
		panic(err)
	}

	filtered := make([]config.TestBlueprint, 0, len(allTests)*count)
	for _, test := range allTests {
		if !re.MatchString(test.Name) {
			continue
		}

		fmt.Println("loaded:", test.Name)
		for i := 0; i < count; i++ {
			filtered = append(filtered, test)
		}
	}

	return filtered
}
