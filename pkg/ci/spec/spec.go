package spec

type JobInfo struct {
	Name        string `json:"name"`
	Stage       string `json:"stage"`
	ProjectID   int64  `json:"project_id"`
	ProjectName string `json:"project_name"`

	TimeInQueueSeconds                       float64 `json:"time_in_queue_seconds"`
	ProjectJobsRunningOnInstanceRunnersCount string  `json:"project_jobs_running_on_instance_runners_count"`
}

type GitInfo struct {
	RepoURL   string   `json:"repo_url"`
	Ref       string   `json:"ref"`
	Sha       string   `json:"sha"`
	BeforeSha string   `json:"before_sha"`
	RefType   string   `json:"ref_type"`
	Refspecs  []string `json:"refspecs"`
	Depth     int      `json:"depth"`
}

type RunnerInfo struct {
	Timeout int `json:"timeout"`
}

type Variable struct {
	Key    string `json:"key"`
	Value  string `json:"value"`
	Public bool   `json:"public"`
	File   bool   `json:"file"`
	Masked bool   `json:"masked"`
	Raw    bool   `json:"raw"`
}

type Step struct {
	Name         string   `json:"name"`
	Script       []string `json:"script"`
	Timeout      int      `json:"timeout"`
	When         string   `json:"when"`
	AllowFailure bool     `json:"allow_failure"`
}

type Image struct {
	Name            string         `json:"name"`
	Alias           string         `json:"alias,omitempty"`
	Command         []string       `json:"command,omitempty"`
	Entrypoint      []string       `json:"entrypoint,omitempty"`
	Ports           []Port         `json:"ports,omitempty"`
	Variables       []Variable     `json:"variables,omitempty"`
	PullPolicies    []string       `json:"pull_policy,omitempty"`
	ExecutorOptions map[string]any `json:"executor_opts,omitempty"`
}

type Port struct {
	Number   int    `json:"number,omitempty"`
	Protocol string `json:"protocol,omitempty"`
	Name     string `json:"name,omitempty"`
}

type Artifact struct {
	Name      string   `json:"name"`
	Untracked bool     `json:"untracked"`
	Paths     []string `json:"paths"`
	Exclude   []string `json:"exclude"`
	When      string   `json:"when"`
	Type      string   `json:"artifact_type"`
	Format    string   `json:"artifact_format"`
	ExpireIn  string   `json:"expire_in"`
}

type Cache struct {
	Key          string   `json:"key"`
	Untracked    bool     `json:"untracked"`
	Policy       string   `json:"policy"`
	Paths        []string `json:"paths"`
	When         string   `json:"when"`
	FallbackKeys []string `json:"fallback_keys"`
}

type Credentials struct {
	Type     string `json:"type"`
	URL      string `json:"url"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type DependencyArtifactsFile struct {
	Filename string `json:"filename"`
	Size     int64  `json:"size"`
}

type Dependency struct {
	ID            int64                   `json:"id"`
	Token         string                  `json:"token"`
	Name          string                  `json:"name"`
	ArtifactsFile DependencyArtifactsFile `json:"artifacts_file"`
}

type GitlabFeatures struct {
	TraceSections     bool     `json:"trace_sections"`
	TokenMaskPrefixes []string `json:"token_mask_prefixes"`
	FailureReasons    []string `json:"failure_reasons"`
}

type Hook struct {
	Name   string   `json:"name"`
	Script []string `json:"script"`
}

type Job struct {
	ID            int64             `json:"id"`
	Token         string            `json:"token"`
	AllowGitFetch bool              `json:"allow_git_fetch"`
	JobInfo       JobInfo           `json:"job_info"`
	GitInfo       GitInfo           `json:"git_info"`
	RunnerInfo    RunnerInfo        `json:"runner_info"`
	Variables     []Variable        `json:"variables"`
	Steps         []Step            `json:"steps"`
	Image         Image             `json:"image"`
	Services      []Image           `json:"services"`
	Artifacts     []Artifact        `json:"artifacts"`
	Cache         []Cache           `json:"cache"`
	Credentials   []Credentials     `json:"credentials"`
	Dependencies  []Dependency      `json:"dependencies"`
	Features      GitlabFeatures    `json:"features"`
	Secrets       map[string]Secret `json:"secrets,omitempty"`
	Hooks         []Hook            `json:"hooks,omitempty"`
}

type Secret struct {
	Vault            *VaultSecret            `json:"vault,omitempty"`
	GCPSecretManager *GCPSecretManagerSecret `json:"gcp_secret_manager,omitempty"`
	AzureKeyVault    *AzureKeyVaultSecret    `json:"azure_key_vault,omitempty"`
	File             *bool                   `json:"file,omitempty"`
}

type GCPSecretManagerSecret struct {
	Name    string                 `json:"name"`
	Version string                 `json:"version"`
	Server  GCPSecretManagerServer `json:"server"`
}

type GCPSecretManagerServer struct {
	ProjectNumber                        string `json:"project_number"`
	WorkloadIdentityFederationPoolId     string `json:"workload_identity_federation_pool_id"`
	WorkloadIdentityFederationProviderID string `json:"workload_identity_federation_provider_id"`
	JWT                                  string `json:"jwt"`
}

type AzureKeyVaultSecret struct {
	Name    string              `json:"name"`
	Version string              `json:"version,omitempty"`
	Server  AzureKeyVaultServer `json:"server"`
}

type AzureKeyVaultServer struct {
	ClientID string `json:"client_id"`
	TenantID string `json:"tenant_id"`
	JWT      string `json:"jwt"`
	URL      string `json:"url"`
}

type VaultSecret struct {
	Server VaultServer `json:"server"`
	Engine VaultEngine `json:"engine"`
	Path   string      `json:"path"`
	Field  string      `json:"field"`
}

type VaultServer struct {
	URL       string    `json:"url"`
	Auth      VaultAuth `json:"auth"`
	Namespace string    `json:"namespace"`
}

type VaultAuth struct {
	Name string                 `json:"name"`
	Path string                 `json:"path"`
	Data map[string]interface{} `json:"data"`
}

type VaultEngine struct {
	Name string `json:"name"`
	Path string `json:"path"`
}
