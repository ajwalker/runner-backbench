package features

// sInfo contains information about what features are supported.
type Info struct {
	Variables               bool `json:"variables" expr:"variables"`
	Artifacts               bool `json:"artifacts" expr:"artifacts"`
	Cache                   bool `json:"cache" expr:"cache"`
	FallbackCacheKeys       bool `json:"fallback_cache_keys" expr:"fallback_cache_keys"`
	UploadMultipleArtifacts bool `json:"upload_multiple_artifacts" expr:"upload_multiple_artifacts"`
	UploadRawArtifacts      bool `json:"upload_raw_artifacts" expr:"upload_raw_artifacts"`
	Terminal                bool `json:"terminal" expr:"terminal"`
	Refspecs                bool `json:"refspecs" expr:"refspecs"`
	Masking                 bool `json:"masking" expr:"masking"`
	RawVariables            bool `json:"raw_variables" expr:"raw_variables"`
	ArtifactsExclude        bool `json:"artifacts_exclude" expr:"artifacts_exclude"`
	MultiBuildSteps         bool `json:"multi_build_steps" expr:"multi_build_steps"`
	TraceReset              bool `json:"trace_reset" expr:"trace_reset"`
	TraceChecksum           bool `json:"trace_checksum" expr:"trace_checksum"`
	TraceSize               bool `json:"trace_size" expr:"trace_size"`
	VaultSecrets            bool `json:"vault_secrets" expr:"vault_secrets"`
	Cancelable              bool `json:"cancelable" expr:"cancelable"`
	ReturnExitCode          bool `json:"return_exit_code" expr:"return_exit_code"`
	ServiceVariables        bool `json:"service_variables" expr:"service_variables"`
	ServiceMultipleAliases  bool `json:"service_multiple_aliases" expr:"service_multiple_aliases"`
	ImageExecutorOpts       bool `json:"image_executor_opts" expr:"image_executor_opts"`
	ServiceExecutorOpts     bool `json:"service_executor_opts" expr:"service_executor_opts"`

	Image    bool `json:"image" expr:"image"`
	Services bool `json:"services" expr:"services"`
	Shared   bool `json:"shared" expr:"shared"`
	Proxy    bool `json:"proxy" expr:"proxy"`
	Session  bool `json:"session" expr:"session"`
}
