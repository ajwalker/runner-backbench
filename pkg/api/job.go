package api

import (
	"sync"

	"gitlab.com/ajwalker/runner-backbench/pkg/config"
)

type jobState string

const (
	Running   jobState = "running"
	Failed    jobState = "failed"
	Success   jobState = "success"
	Canceled  jobState = "canceled"
	Canceling jobState = "canceling"
)

type inflight struct {
	mu   sync.Mutex
	test *config.RenderedTest

	state         jobState
	failureReason string
	exitCode      int
	watching      bool
	trace         []byte
	checksum      string
	artifacts     map[string]Artifact
}

type Artifact struct {
	ExpireIn string
	Type     string
	Format   string
	Content  []byte
}

func (j *inflight) setState(state jobState, failure string) {
	j.mu.Lock()
	defer j.mu.Unlock()

	j.state = state
	j.failureReason = failure
}

// State returns the job's state.
func (j *inflight) State() jobState {
	j.mu.Lock()
	defer j.mu.Unlock()

	return j.state
}

// FailureReason returns the job's reason for failure.
func (j *inflight) FailureReason() string {
	j.mu.Lock()
	defer j.mu.Unlock()

	return j.failureReason
}

func (j *inflight) appendTrace(p []byte) {
	j.trace = append(j.trace, p...)
}

func (j *inflight) getTraceSize() int64 {
	j.mu.Lock()
	defer j.mu.Unlock()

	return int64(len(j.trace))
}

func (j *inflight) setTraceChecksum(checksum string) {
	j.mu.Lock()
	defer j.mu.Unlock()

	j.checksum = checksum
}

func (j *inflight) setExitCode(code int) {
	j.mu.Lock()
	defer j.mu.Unlock()

	j.exitCode = code
}

func (j *inflight) ExitCode() int {
	j.mu.Lock()
	defer j.mu.Unlock()

	return j.exitCode
}

// Watch sets whether a job's log is actively being watched.
func (j *inflight) Watch(watching bool) {
	j.mu.Lock()
	defer j.mu.Unlock()

	j.watching = watching
}

// IsWatched returns true if a job's log is actively being watched.
func (j *inflight) IsWatched() bool {
	j.mu.Lock()
	defer j.mu.Unlock()

	return j.watching
}

// Trace returns the current trace data.
func (j *inflight) Trace() []byte {
	j.mu.Lock()
	defer j.mu.Unlock()

	return j.trace
}

// Checksum returns the trace's checksum
func (j *inflight) Checksum() string {
	j.mu.Lock()
	defer j.mu.Unlock()

	return j.checksum
}

// Cancel cancels the job. This is similar to pressing the Cancel button on a
// job in GitLab's UI.
func (j *inflight) Cancel() {
	j.setState("canceling", "")
}

// Abort aborts the job.
func (j *inflight) Abort() {
	// failure reason and exit code is handled server side, as when a job
	// is entirely aborted, runner cannot actually update the job anymore
	// because the token is invalidated
	j.setState("canceled", "job_canceled")
	j.setExitCode(1)
}

func (j *inflight) addArtifact(name string, a Artifact) {
	j.mu.Lock()
	defer j.mu.Unlock()

	if j.artifacts == nil {
		j.artifacts = make(map[string]Artifact)
	}

	j.artifacts[name] = a
}

// Artifact returns a job's artifact by name.
func (j *inflight) Artifact(name string) Artifact {
	j.mu.Lock()
	defer j.mu.Unlock()

	return j.artifacts[name]
}

func (j *inflight) Test() *config.RenderedTest {
	return j.test
}
