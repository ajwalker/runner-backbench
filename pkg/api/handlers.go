package api

import (
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"slices"
	"strconv"
	"strings"
	"time"

	"gitlab.com/ajwalker/runner-backbench/pkg/api/internal/network"
	"gitlab.com/ajwalker/runner-backbench/pkg/ci/spec"
	"gitlab.com/ajwalker/runner-backbench/pkg/config"
)

func (s *Server) handleRunners(rw http.ResponseWriter, r *http.Request) {
	if !isValidContentType(rw, r) {
		return
	}

	switch r.Method {
	// Registers a new Runner
	// https://gitlab.com/gitlab-org/gitlab/-/blob/v14.5.2-ee/lib/api/ci/runner.rb#L11
	case http.MethodPost:
		http.Error(rw, http.StatusText(http.StatusNotImplemented), http.StatusNotImplemented)
		return

	// Deletes a registered Runner
	// https://gitlab.com/gitlab-org/gitlab/-/blob/v14.5.2-ee/lib/api/ci/runner.rb#L54
	case http.MethodDelete:
		http.Error(rw, http.StatusText(http.StatusNotImplemented), http.StatusNotImplemented)
		return

	default:
		http.Error(rw, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}
}

// handleJobRequest requests a new job
// https://gitlab.com/gitlab-org/gitlab/-/blob/v14.5.2-ee/lib/api/ci/runner.rb#L82
func (s *Server) handleJobRequest(rw http.ResponseWriter, r *http.Request) {
	if !isValidContentType(rw, r) {
		return
	}

	if r.Method != http.MethodPost {
		http.Error(rw, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	var request network.RequestJobRequest
	if !isValidPayload(&request, rw, r) {
		return
	}

	s.mu.Lock()
	if s.start.IsZero() {
		s.start = time.Now()
	}

	var test *config.RenderedTest
	var id int64
	var err error
	for len(s.blueprints) > 0 {
		blueprint := s.blueprints[0]
		s.blueprints = s.blueprints[1:]

		test, err = blueprint.Render(request.Info)
		if err != nil {
			fmt.Printf("blueprint error: %s: %v\n", blueprint.Name, err)
			continue
		}

		// make sure job matches request features
		if !match(request.Info, test) {
			fmt.Println("skipped!:", blueprint.Name)
			continue
		}

		s.nextJobID++
		id = s.nextJobID
		break
	}
	s.mu.Unlock()

	if id == 0 {
		rw.WriteHeader(http.StatusNoContent)
		return
	}

	fmt.Println("sending", test.Name)

	job := test.Job
	job.ID = id
	job.Token = generateJobToken()
	job.Variables = append(
		test.Job.Variables,
		spec.Variable{
			Key:   "CI_SERVER_URL",
			Value: "https://" + s.addr,
		},
		spec.Variable{
			Key:    "CI_JOB_TOKEN",
			Value:  test.Job.Token,
			Masked: true,
		},
		spec.Variable{
			Key:   config.ScriptCancelCommandFunc,
			Value: config.ScriptCancelCommand,
		},
		spec.Variable{
			Key:   config.ScriptAbortCommandFunc,
			Value: config.ScriptAbortCommand,
		},
	)

	s.mu.Lock()
	s.inflight[id] = &inflight{test: test, state: "running"}
	s.mu.Unlock()

	rw.Header().Add("Content-Type", "application/json")
	rw.WriteHeader(http.StatusCreated)

	json.NewEncoder(rw).Encode(job)
}

func (s *Server) handleJobs(rw http.ResponseWriter, r *http.Request) {
	path := strings.TrimPrefix(r.URL.Path, "/api/v4/jobs/")
	idx := strings.Index(path+"/", "/")
	id, _ := strconv.ParseInt(path[:idx], 10, 64)
	path = path[idx:]

	job := s.Job(id)
	if job == nil {
		http.Error(rw, http.StatusText(http.StatusForbidden), http.StatusForbidden)
		return
	}

	switch path {
	case "":
		switch r.Method {
		// Updates a job
		// https://gitlab.com/gitlab-org/gitlab/-/blob/v14.5.2-ee/lib/api/ci/runner.rb#L160
		case http.MethodPut:
			s.handleTracePing(job, rw, r)
			return

		default:
			http.Error(rw, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
			return
		}
	case "/trace":
		switch r.Method {
		// Appends a patch to the job trace
		// https://gitlab.com/gitlab-org/gitlab/-/blob/v14.5.2-ee/lib/api/ci/runner.rb#L195
		case http.MethodPatch:
			s.handleTracePatch(job, rw, r)
			return

		default:
			http.Error(rw, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
			return
		}

	case "/artifacts":
		switch r.Method {
		// Upload artifacts for job
		// https://gitlab.com/gitlab-org/gitlab/-/blob/v14.5.2-ee/lib/api/ci/runner.rb#L267 (proxied via workhorse)
		case http.MethodPost:
			s.handleArtifactPost(job, rw, r)

		// Download the artifacts file for job
		// https://gitlab.com/gitlab-org/gitlab/-/blob/v14.5.2-ee/lib/api/ci/runner.rb#L305
		case http.MethodGet:
			s.handleArtifactGet(job, rw, r)
		default:
			http.Error(rw, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
			return
		}

	default:
		panic(path)
	}
}

func (s *Server) handleTracePing(job *inflight, rw http.ResponseWriter, r *http.Request) {
	if !isValidContentType(rw, r) {
		return
	}

	var request network.TracePingRequest
	if !isValidPayload(&request, rw, r) {
		return
	}

	if !isJobRunning(job, rw, r, true) {
		return
	}

	switch jobState(request.State) {
	case Running, Failed, Success:
	default:
		http.Error(rw, http.StatusText(http.StatusNotImplemented), http.StatusNotImplemented)
		return
	}

	job.setState(jobState(request.State), request.FailureReason)
	if request.State != "running" {
		job.setTraceChecksum(request.Output.Checksum)
		job.setExitCode(request.ExitCode)

		rw.WriteHeader(http.StatusOK)
		return
	}

	if job.IsWatched() {
		rw.Header().Set("X-GitLab-Trace-Update-Interval", "5")
	} else {
		rw.Header().Set("X-GitLab-Trace-Update-Interval", "10") // GitLab is usually 60
	}
	rw.WriteHeader(http.StatusAccepted)
}

func (s *Server) handleTracePatch(job *inflight, rw http.ResponseWriter, r *http.Request) {
	if !isValidToken(job, rw, r) {
		return
	}

	if !isJobRunning(job, rw, r, true) {
		return
	}

	start, end, ok := parseContentRange(r.Header.Get("Content-Range"))
	if !ok {
		http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	body, err := io.ReadAll(io.LimitReader(r.Body, 2*1024*1024))
	if err != nil {
		http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	if len(body) > 1024*1024 {
		http.Error(rw, http.StatusText(http.StatusRequestEntityTooLarge), http.StatusRequestEntityTooLarge)
		return
	}

	// https://gitlab.com/gitlab-org/gitlab-runner/issues/3275 fixed?
	if len(body) != int(end-start+1) {
		http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	if end-start > 125*1024*1024 {
		job.setState("failure", "trace_size_exceeded")
		http.Error(rw, http.StatusText(http.StatusForbidden), http.StatusForbidden)
		return
	}

	if job.getTraceSize() != start {
		rw.Header().Set("Range", fmt.Sprintf("0-%d", job.getTraceSize()))
		http.Error(rw, http.StatusText(http.StatusRequestedRangeNotSatisfiable), http.StatusRequestedRangeNotSatisfiable)
		return
	}

	job.appendTrace(body)

	rw.Header().Set("Job-Status", string(job.State()))
	rw.Header().Set("Range", fmt.Sprintf("0-%d", job.getTraceSize()))
	if job.IsWatched() {
		rw.Header().Set("X-GitLab-Trace-Update-Interval", "5")
	} else {
		// GitLab is 60s, but we need more frequent updates for matching text against
		rw.Header().Set("X-GitLab-Trace-Update-Interval", "10")
	}

	rw.WriteHeader(http.StatusAccepted)
}

func parseContentRange(cr string) (int64, int64, bool) {
	r := strings.SplitN(strings.TrimSpace(cr), "-", 2)
	if len(r) < 2 {
		return 0, 0, false
	}

	start, err := strconv.ParseInt(r[0], 10, 64)
	if err != nil {
		return 0, 0, false
	}
	end, err := strconv.ParseInt(r[1], 10, 64)
	if err != nil {
		return 0, 0, false
	}

	return start, end, true
}

func (s *Server) handleArtifactPost(job *inflight, rw http.ResponseWriter, r *http.Request) {
	if !isValidToken(job, rw, r) {
		return
	}

	if !isJobRunning(job, rw, r, false) {
		return
	}

	f, fh, err := r.FormFile("file")
	if err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	defer f.Close()

	content, err := io.ReadAll(f)
	if err != nil {
		http.Error(rw, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	job.addArtifact(fh.Filename, Artifact{
		ExpireIn: r.URL.Query().Get("expire_in"),
		Type:     r.URL.Query().Get("artifact_type"),
		Format:   r.URL.Query().Get("artifact_format"),
		Content:  content,
	})
}

func (s *Server) handleArtifactGet(job *inflight, rw http.ResponseWriter, r *http.Request) {
	if !isValidToken(job, rw, r) {
		return
	}

	if !isJobRunning(job, rw, r, false) {
		return
	}

	for _, a := range job.artifacts {
		if a.Type == "archive" {
			_, err := rw.Write(a.Content)
			if err != nil {
				http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
				return
			}
		}
	}
}

func isValidContentType(rw http.ResponseWriter, r *http.Request) bool {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, http.StatusText(http.StatusUnsupportedMediaType), http.StatusUnsupportedMediaType)

		return false
	}

	return true
}

func isValidToken(job *inflight, rw http.ResponseWriter, r *http.Request) bool {
	return true
}

func isJobRunning(job *inflight, rw http.ResponseWriter, r *http.Request, setJobStatus bool) bool {
	state := job.State()

	if state == "canceling" {
		if setJobStatus {
			rw.Header().Set("Job-Status", string(state))
		}

		return true
	}

	if state != "running" {
		if setJobStatus {
			rw.Header().Set("Job-Status", string(state))
		}

		http.Error(rw, http.StatusText(http.StatusForbidden), http.StatusForbidden)

		return false
	}

	return true
}

func isValidPayload(request interface{}, rw http.ResponseWriter, r *http.Request) bool {
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

		return false
	}

	return true
}

func generateJobToken() string {
	var b [32]byte
	rand.Read(b[:])
	return hex.EncodeToString(b[:])
}

func match(info network.VersionInfo, test *config.RenderedTest) bool {
	if len(test.Runner.Shell) > 0 && !slices.Contains(test.Runner.Shell, info.Shell) {
		return false
	}

	if len(test.Runner.Executor) > 0 && !slices.Contains(test.Runner.Executor, info.Executor) {
		return false
	}

	return (!test.Runner.Features.Variables && info.Features.Variables) ||
		(!test.Runner.Features.Image && info.Features.Image) ||
		(!test.Runner.Features.Services && info.Features.Services) ||
		(!test.Runner.Features.Artifacts && info.Features.Artifacts) ||
		(!test.Runner.Features.Cache && info.Features.Cache) ||
		(!test.Runner.Features.FallbackCacheKeys && info.Features.FallbackCacheKeys) ||
		(!test.Runner.Features.Shared && info.Features.Shared) ||
		(!test.Runner.Features.UploadMultipleArtifacts && info.Features.UploadMultipleArtifacts) ||
		(!test.Runner.Features.UploadRawArtifacts && info.Features.UploadRawArtifacts) ||
		(!test.Runner.Features.Session && info.Features.Session) ||
		(!test.Runner.Features.Terminal && info.Features.Terminal) ||
		(!test.Runner.Features.Refspecs && info.Features.Refspecs) ||
		(!test.Runner.Features.Masking && info.Features.Masking) ||
		(!test.Runner.Features.Proxy && info.Features.Proxy) ||
		(!test.Runner.Features.RawVariables && info.Features.RawVariables) ||
		(!test.Runner.Features.ArtifactsExclude && info.Features.ArtifactsExclude) ||
		(!test.Runner.Features.MultiBuildSteps && info.Features.MultiBuildSteps) ||
		(!test.Runner.Features.TraceReset && info.Features.TraceReset) ||
		(!test.Runner.Features.TraceChecksum && info.Features.TraceChecksum) ||
		(!test.Runner.Features.TraceSize && info.Features.TraceSize) ||
		(!test.Runner.Features.VaultSecrets && info.Features.VaultSecrets) ||
		(!test.Runner.Features.Cancelable && info.Features.Cancelable) ||
		(!test.Runner.Features.ReturnExitCode && info.Features.ReturnExitCode) ||
		(!test.Runner.Features.ServiceVariables && info.Features.ServiceVariables) ||
		(!test.Runner.Features.ServiceMultipleAliases && info.Features.ServiceMultipleAliases) ||
		(!test.Runner.Features.ImageExecutorOpts && info.Features.ImageExecutorOpts) ||
		(!test.Runner.Features.ServiceExecutorOpts && !info.Features.ServiceExecutorOpts)
}
