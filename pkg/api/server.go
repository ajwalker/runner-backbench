package api

import (
	"errors"
	"fmt"
	"net/http"
	"regexp"
	"strings"
	"sync"
	"time"
	"unicode"

	"gitlab.com/ajwalker/runner-backbench/pkg/config"
)

type Server struct {
	addr       string
	blueprints []config.TestBlueprint
	mux        *http.ServeMux

	mu        sync.Mutex
	inflight  map[int64]*inflight
	nextJobID int64

	start time.Time
}

func New(addr string, tests []config.TestBlueprint) *Server {
	s := &Server{
		addr:     addr,
		mux:      http.NewServeMux(),
		inflight: make(map[int64]*inflight),
	}

	s.blueprints = append(s.blueprints, tests...)

	s.mux.HandleFunc("/api/v4/runners", s.handleRunners)
	s.mux.HandleFunc("/api/v4/jobs/request", s.handleJobRequest)
	s.mux.HandleFunc("/api/v4/jobs/", s.handleJobs)

	go s.housekeeping()

	return s
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.mux.ServeHTTP(w, r)
}

func (s *Server) Job(id int64) *inflight {
	s.mu.Lock()
	defer s.mu.Unlock()

	return s.inflight[id]
}

func (s *Server) housekeeping() {
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	for range ticker.C {
		if s.collect() {
			return
		}
	}
}

func (s *Server) collect() bool {
	s.mu.Lock()
	defer s.mu.Unlock()

	for id, job := range s.inflight {
		switch job.State() {
		case Success, Failed, Canceled:
			s.eval(job)
			delete(s.inflight, id)
			if len(s.inflight) == 0 {
				fmt.Println("done", time.Since(s.start))
			}

		case Running:
			if job.test.WillCancel || job.test.WillAbort {
				trace := string(job.Trace())
				if job.test.WillCancel {
					s.findAndCommand(config.ScriptCancelCommand, trace, job.Cancel)
				}
				if job.test.WillAbort {
					s.findAndCommand(config.ScriptAbortCommand, trace, job.Abort)
				}
			}
		}
	}

	return len(s.blueprints) == 0 && len(s.inflight) == 0
}

func (s *Server) findAndCommand(command, trace string, cancel func()) {
	if idx := strings.Index(trace, command); idx > -1 {
		find := trace[idx+len(command) : idx+strings.IndexFunc(trace[idx:], func(r rune) bool {
			return !unicode.IsPrint(r)
		})]
		off := idx + len(command) + len(find)
		find = strings.Trim(find, ":")

		if strings.Contains(trace[off:], find) {
			cancel()
		}
	}
}

func (s *Server) eval(job *inflight) {
	test := job.Test()

	var errs []error
	if test.FailureReason != job.FailureReason() {
		errs = append(errs, fmt.Errorf("expected failure reason %s, got %s", test.FailureReason, job.FailureReason()))
	}

	if test.ExitCode != job.ExitCode() {
		errs = append(errs, fmt.Errorf("expected exit code %d, got %d", test.ExitCode, job.ExitCode()))
	}

	trace := string(job.Trace())
	for _, include := range test.Includes {
		if !strings.Contains(trace, include) {
			errs = append(errs, fmt.Errorf("trace did not include %q", include))
		}
	}

	for _, exclude := range test.Excludes {
		if strings.Contains(trace, exclude) {
			errs = append(errs, fmt.Errorf("trace did not exclude %q", exclude))
		}
	}

	for _, match := range test.Match {
		matched, _ := regexp.MatchString(match, trace)
		if !matched {
			errs = append(errs, fmt.Errorf("trace did not match %q", match))
		}
	}

	if len(errs) == 0 {
		fmt.Println("pass:", job.test.Name)
	} else {
		fmt.Println("fail:", job.test.Name)
		fmt.Println(errors.Join(errs...))
		fmt.Println("trace:")
		fmt.Println(trace)
	}
}
