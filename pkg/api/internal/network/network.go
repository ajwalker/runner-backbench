package network

import "gitlab.com/ajwalker/runner-backbench/pkg/internal/features"

// VersionInfo contains information about the runner's version, supported
// features and executor used.
type VersionInfo struct {
	RunnerInfo
	Features features.Info `json:"features" expr:"features"`
}

type RunnerInfo struct {
	Name         string `json:"name,omitempty" expr:"name"`
	Version      string `json:"version,omitempty" expr:"version"`
	Revision     string `json:"revision,omitempty" expr:"revision"`
	Platform     string `json:"platform,omitempty" expr:"platform"`
	Architecture string `json:"architecture,omitempty" expr:"architecture"`
	Executor     string `json:"executor,omitempty" expr:"executor"`
	Shell        string `json:"shell,omitempty" expr:"shell"`
}

type RequestJobRequest struct {
	Info       VersionInfo              `json:"info"`
	Token      string                   `json:"token"`
	LastUpdate string                   `json:"last_update"`
	Session    RequestJobRequestSession `json:"session"`
}

type RequestJobRequestSession struct {
	URL           string `json:"url,omitempty"`
	Certificate   string `json:"certificate,omitempty"`
	Authorization string `json:"authorization,omitempty"`
}

type TracePingRequest struct {
	Info          VersionInfo `json:"info"`
	Token         string      `json:"token"`
	State         string      `json:"state"`
	FailureReason string      `json:"failure_reason,omitempty"`
	ExitCode      int         `json:"exit_code,omitempty"`

	Checksum string                 `json:"checksum,omitempty"` // deprecated
	Output   TracePingRequestOutput `json:"output,omitempty"`
}

type TracePingRequestOutput struct {
	Checksum string `json:"checksum"`
	Bytesize int64  `json:"bytesize"`
}
