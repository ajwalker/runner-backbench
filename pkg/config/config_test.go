package config

import (
	"os"
	"testing"
)

func TestConfig(t *testing.T) {
	f, err := os.Open("../../jobs/multistep.yaml")
	if err != nil {
		t.Fatal(err)
	}
	defer f.Close()

	blueprints, err := New(f, "multistep")
	if err != nil {
		t.Fatal(err)
	}

	if len(blueprints) != 3 {
		t.Fatalf("expected test length to be 3, got %d", len(blueprints))
	}

	_, err = blueprints[0].Render(nil)
	if err != nil {
		t.Fatal(err)
	}
}
