package gitlab

type Job struct {
	Variables    map[string]string `yaml:"variables,omitempty"`
	Image        Image             `yaml:"image,omitempty"`
	BeforeScript []string          `yaml:"before_script,omitempty"`
	Script       []string          `yaml:"script,omitempty"`
	AfterScript  []string          `yaml:"after_script,omitempty"`
	Timeout      string            `yaml:"timeout,omitempty"`
}

type Image struct {
	Name       string   `yaml:"name,omitempty"`
	Alias      string   `yaml:"alias,omitempty"`
	Command    []string `yaml:"command,omitempty"`
	Entrypoint []string `yaml:"entrypoint,omitempty"`
}
