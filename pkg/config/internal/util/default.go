package util

func Default[T comparable](v *T, override T) {
	if v == nil {
		return
	}

	var empty T
	if *v == empty {
		*v = override
	}
}
