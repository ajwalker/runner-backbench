package config

import (
	"fmt"
	"strings"

	"gitlab.com/ajwalker/runner-backbench/pkg/config/internal/gitlab"
	"gopkg.in/yaml.v3"
)

func ConvertToGitLab(blueprints []TestBlueprint) string {
	document := new(yaml.Node)
	document.Kind = yaml.MappingNode

	var sb strings.Builder
	for _, blueprint := range blueprints {
		test, err := blueprint.Render(nil)
		if err != nil {
			panic(err)
		}

		sb.Reset()

		if test.Job.GitInfo.Depth > 0 {
			sb.WriteString(fmt.Sprintf("# set project's Git shallow clone value to: %v\n", test.Job.GitInfo.Depth))
		}
		for _, include := range test.Includes {
			sb.WriteString(fmt.Sprintf("# check includes: %q\n", include))
		}
		for _, exclude := range test.Excludes {
			sb.WriteString(fmt.Sprintf("# check excludes: %q\n", exclude))
		}
		for _, match := range test.Match {
			sb.WriteString(fmt.Sprintf("# check matches: %v\n", match))
		}
		if test.FailureReason != "" {
			sb.WriteString(fmt.Sprintf("# check failure reason is: %v\n", test.FailureReason))
		}
		if test.ExitCode > 0 {
			sb.WriteString(fmt.Sprintf("# check exit code is: %v\n", test.ExitCode))
		}

		job := gitlab.Job{Variables: make(map[string]string)}

		if test.Job.RunnerInfo.Timeout > 0 {
			job.Timeout = fmt.Sprintf("%d seconds", test.Job.RunnerInfo.Timeout)
		}

		if test.Job.Image.Name != "" {
			job.Image = gitlab.Image{
				Name:       test.Job.Image.Name,
				Alias:      test.Job.Image.Alias,
				Command:    test.Job.Image.Command,
				Entrypoint: test.Job.Image.Entrypoint,
			}
		}
		for _, step := range test.Job.Steps {
			switch step.Name {
			case "before_script":
				job.AfterScript = step.Script
			case "script":
				job.Script = step.Script
			case "after_script":
				job.AfterScript = step.Script
			}
		}

		for _, variable := range test.Job.Variables {
			job.Variables[variable.Key] = variable.Value
		}

		node := new(yaml.Node)
		if err := node.Encode(job); err != nil {
			panic(err)
		}

		document.Content = append(document.Content, &yaml.Node{Kind: yaml.ScalarNode, Value: blueprint.Name, HeadComment: "\n" + sb.String()})
		document.Content = append(document.Content, node)
	}

	out, err := yaml.Marshal(document)
	if err != nil {
		panic(err)
	}

	return string(out)
}
