package config

import (
	"bytes"
	_ "embed"
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"path"
	"strconv"
	"strings"

	"github.com/expr-lang/expr"
	"gopkg.in/yaml.v3"

	"gitlab.com/ajwalker/runner-backbench/pkg/ci/spec"
	"gitlab.com/ajwalker/runner-backbench/pkg/config/internal/util"
	"gitlab.com/ajwalker/runner-backbench/pkg/internal/features"
)

const (
	ScriptCancelCommandFunc = "backbench_cancel_func"
	ScriptAbortCommandFunc  = "backbench_abort_func"

	ScriptCancelCommand = "backbench_api:cancel"
	ScriptAbortCommand  = "backbench_api:abort"
)

type TestBlueprint struct {
	Name string `json:"name"`

	node     *yaml.Node
	rendered *RenderedTest
}

type RenderedTest struct {
	Name string `json:"-"`

	Job spec.Job `json:"job"`

	Match         []string `json:"match"`
	Includes      []string `json:"includes"`
	Excludes      []string `json:"excludes"`
	ExitCode      int      `json:"exit_code"`
	FailureReason string   `json:"failure_reason"`

	// signal that the job is expected to be cancelled via special trace output
	WillCancel bool `json:"-"`
	WillAbort  bool `json:"-"`

	Runner struct {
		Features features.Info `json:"features"`
		Shell    []string
		Executor []string
	} `json:"runner"`
}

func Load(fsys fs.FS) ([]TestBlueprint, error) {
	var tests []TestBlueprint

	matches, _ := fs.Glob(fsys, "*.yaml")
	for _, match := range matches {
		f, err := fsys.Open(match)
		if err != nil {
			return nil, fmt.Errorf("opening file: %w", err)
		}

		blueprints, err := New(f, strings.TrimSuffix(match, path.Ext(match)))
		f.Close()
		if err != nil {
			return nil, fmt.Errorf("reading file: %w", err)
		}

		tests = append(tests, blueprints...)
	}

	return tests, nil
}

func New(r io.Reader, name string) ([]TestBlueprint, error) {
	var nodes map[string]yaml.Node

	dec := yaml.NewDecoder(r)
	if err := dec.Decode(&nodes); err != nil {
		return nil, err
	}

	var tests []TestBlueprint
	for testName := range nodes {
		fqn := name + "/" + strings.ReplaceAll(testName, " ", "_")

		node := nodes[testName]
		tests = append(tests, TestBlueprint{Name: fqn, node: &node})
	}

	return tests, nil
}

func (t *TestBlueprint) Render(input any) (*RenderedTest, error) {
	if t.rendered != nil {
		return t.rendered, nil
	}

	if err := eval(t.node, input); err != nil {
		return nil, fmt.Errorf("eval: %w", err)
	}

	var m any
	if err := t.node.Decode(&m); err != nil {
		return nil, fmt.Errorf("decoding: %w", err)
	}

	encoded, err := json.Marshal(m)
	if err != nil {
		return nil, err
	}

	var rendered RenderedTest

	decoder := json.NewDecoder(bytes.NewReader(encoded))
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&rendered); err != nil {
		return nil, err
	}

	// cache the result
	t.rendered = &rendered
	t.rendered.Name = t.Name

	return populate(t.rendered), nil
}

func populate(test *RenderedTest) *RenderedTest {
	util.Default(&test.Job.JobInfo.Name, "test job")
	util.Default(&test.Job.JobInfo.Stage, "test")
	util.Default(&test.Job.JobInfo.ProjectName, "test project")

	util.Default(&test.Job.GitInfo.RepoURL, "https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-pipeline-tests/gitlab-test.git")
	util.Default(&test.Job.GitInfo.Sha, "69b18e5ed3610cf646119c3e38f462c64ec462b7")
	util.Default(&test.Job.GitInfo.BeforeSha, "1ea27a9695f80d7816d9e8ce025d9b2df83d0dd7")
	util.Default(&test.Job.GitInfo.Ref, "main")
	util.Default(&test.Job.GitInfo.RefType, "branch")

	if len(test.Job.GitInfo.Refspecs) == 0 {
		test.Job.GitInfo.Refspecs = []string{"+refs/heads/*:refs/origin/heads/*", "+refs/tags/*:refs/tags/*"}
	}

	for idx, step := range test.Job.Steps {
		for _, script := range step.Script {
			if strings.Contains(script, ScriptCancelCommandFunc) {
				test.WillCancel = true
			}

			if strings.Contains(script, ScriptAbortCommandFunc) {
				test.WillAbort = true
			}
		}

		if step.Name == "" {
			test.Job.Steps[idx].Name = "script"
		}

		if step.When == "" {
			test.Job.Steps[idx].When = "always"
		}
	}

	return test
}

func eval(node *yaml.Node, input any) error {
	var visit func(node *yaml.Node) error
	visit = func(node *yaml.Node) error {
		if node == nil {
			return nil
		}

		if node.Kind == yaml.ScalarNode && node.Tag == "!expr" {
			output, err := expr.Eval(node.Value, input)
			if err != nil {
				return err
			}

			switch v := output.(type) {
			case string:
				node.SetString(v)

			case bool:
				node.Value = "false"
				if v {
					node.Value = "true"
				}
				node.Tag = "!!bool"

			case int64:
				node.Value = strconv.FormatInt(v, 10)
				node.Tag = "!!int"

			case int:
				node.Value = strconv.FormatInt(int64(v), 10)
				node.Tag = "!!int"

			case float64:
				node.Value = strconv.FormatFloat(v, 'E', -1, 64)
				node.Tag = "!!float"

			case []string:
				node.Value = ""
				node.Kind = yaml.SequenceNode
				node.Tag = "!!seq"

				for _, item := range v {
					node.Content = append(node.Content, &yaml.Node{Kind: yaml.ScalarNode, Tag: "!!str", Value: item})
				}

			default:
				// todo: support maps (!!map) and sequences (!!seq) and create new child nodes

				panic(fmt.Sprintf("unsupported expression output type: %T (expr: %v)", output, node.Value))
			}

			return nil
		}

		if err := visit(node.Alias); err != nil {
			return err
		}

		for idx := range node.Content {
			if err := visit(node.Content[idx]); err != nil {
				return err
			}
		}

		return nil
	}

	return visit(node)
}
